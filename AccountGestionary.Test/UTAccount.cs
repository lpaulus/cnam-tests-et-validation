using AccountGestionary.Core;
using System;
using Xunit;

namespace AccountGestionary.Test
{
    public class UTAccount 
    {
        private readonly string ibanDefault = "FR8117569000505724247692C04";
        private readonly string ibanWrong = "FRXX";
        private readonly double rateValue = 0.75;
        private readonly Account account;

        public UTAccount()
        {
           account = new Account(ibanDefault);
        }


        

        [Fact]
        public void AccountCreation()
        {
            Assert.NotNull(account);
        }

        [Fact]
        public void AccountHadWrongIban()
        {
            Assert.Throws<ArgumentException>(() => new Account(ibanWrong));
        }

        [Fact]
        public void AccountTakeMoney()
        {
            var moneyOnBalance = 10000.0;
            account.Debit(moneyOnBalance);
            Assert.Equal(-1*moneyOnBalance, account.Balance);
        }

        [Fact]
        public void AccountCheckBaseBalanceValue()
        {
            Assert.Equal<double>(Account.BaseBalance, account.Balance);
        }

        [Fact]
        public void AccountCheckBaseRateValue()
        {
            Assert.Equal(Account.BaseRate, account.Rate);
        }

        [Fact]
        public void AccountSetRate()
        {
            account.SetRate(rateValue);
            Assert.Equal<double>(rateValue, account.Rate);
        }

        [Fact]
        public void CheckAccountSetOpenningDate()
        {
            var openningDate = DateTime.Now.AddDays(-10.0);
            account.SetOpenningDate(openningDate);
            Assert.Equal(openningDate, account.OpeningAccount);
        }

        [Fact]
        public void CheckAccountSetClosingDate()
        {
            var openningDate = DateTime.Now.AddDays(-10.0);
            account.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now.AddDays(+10.0);
            account.SetEndingDate(closingDate);
            Assert.Equal(closingDate, account.EndingAccount);
        }


        [Fact]
        public void CheckAccountSetClosingDateWithout()
        {
            var closingDate = DateTime.Now.AddDays(+10.0);
            account.SetEndingDate(closingDate);
            Assert.Equal(closingDate, account.EndingAccount);
        }

        [Fact]
        public void NonLogicalDate()
        {
            Assert.Throws<InvalidPeriodException>(() =>
           {
               var openningDate = DateTime.Now;
               account.SetOpenningDate(openningDate);
               var closingDate = DateTime.Now.AddDays(-10.0);
               account.SetEndingDate(closingDate);
           });
        }

        [Fact]
        public void CheckDuration()
        {
            int nbYear = 10;
            var openningDate = DateTime.Now;
            account.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now.AddYears(nbYear);
            account.SetEndingDate(closingDate);
            Assert.Equal(nbYear, account.DurationInYears);
        }


        [Fact]
        public void CheckDurationMonth()
        {
            int nbYear = -10;
            int nbMonth = nbYear * -12;
            var openningDate = DateTime.Now.AddYears(nbYear);
            account.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            account.SetEndingDate(closingDate);
            Assert.Equal(nbMonth, account.DurationInMonth);
        }



        [Fact]
        public void CheckAddMoney()
        {
            var moneyOnBalance = 1000.0;
            account.Credit(moneyOnBalance);
            Assert.Equal(moneyOnBalance, account.Balance);

        }
    }
}
