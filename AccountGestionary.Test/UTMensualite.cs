﻿using AccountGestionary.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AccountGestionary.Test
{
    public class UTMensualite
    {
        private readonly int capitalDefault = 200000;
        private readonly string ibanDefault = "FR8117569000505724247692C04";
        private readonly Loan loan;

        public UTMensualite()
        {
            loan = new Loan(ibanDefault, capitalDefault);
            var openningDate = DateTime.Now.AddYears(-25);
            loan.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            loan.SetEndingDate(closingDate);

            loan.Calculation();
        }


        [Fact]
        public void CheckCapitalRemaining()
        {
            Assert.Equal(200000, loan.Mensualities.First().CapitalRemaining, 2);
        }

        [Fact]
        public void CheckInterest()
        {
            Assert.Equal(191.67, loan.Mensualities.First().Interest, 2);
        }

        [Fact]
        public void CheckCapitalBaseRefund()
        {
            Assert.Equal(575.74, loan.Mensualities.First().CapitalRefund, 2);
        }
        [Fact]
        public void CheckAssuranceMensualite()
        {
            Assert.Equal(50.0, loan.Mensualities.First().Assurance, 2);
        }

        [Fact]
        public void CheckGlobalBalanceMensualite()
        {
            Assert.Equal(817.41, loan.Mensualities.First().TotalMensuality, 2);
        }
    }
}
