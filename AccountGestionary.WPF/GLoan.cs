﻿using AccountGestionary.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountGestionary.WPF
{
    class GLoan
    {
        public static ObservableCollection<Loan> Loans = new ObservableCollection<Loan>();



        public static Loan CreateLoan(string iban, double capital)
        {
            var l = new Loan(iban, capital);
            Loans.Add(l);
            return l;
        }
    }

    
}
