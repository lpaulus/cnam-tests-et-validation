﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AccountGestionary.Core
{
    public class Account : ICalculation
    {
        public enum AccountType
        {
            Base,
            Houdini,
            Ldd,
            LivretA,
            PlanEpargneLogement,
            Loan
        }

        public Account(string iban)
        {
            if (!Regex.IsMatch(iban, "^[A-Z]{2}([0-9a-zA-Z]{20})"))
            {
                throw new ArgumentException("iban not correct", "iban");
            }
            Iban = iban;

            OnPeriodChange += Account_OnPeriodChange;
        }



        public string Iban { get; }
        public static double BaseBalance { get; } = 0.0;
        public double Balance { get; protected set; } = BaseBalance;
        public static double BaseRate { get; } = 0.0;
        public double Rate { get; private set;  } = BaseRate;
        public DateTime? OpeningAccount { get; set; } = null;
        public DateTime? EndingAccount { get; set; } = null;
        public double CeillingValue { get; private set; } = Double.MaxValue;

        public event EventHandler OnPeriodChange;
        public int DurationInYears
        {
            get
            {
                if (OpeningAccount == null || EndingAccount == null)
                {
                    return 0;
                }
                var v = new DateTime(EndingAccount.Value.Subtract(OpeningAccount.Value).Ticks);
                return v.Year - 1;
            }
        }
        public int DurationInMonth
        {
            get
            {
                if (OpeningAccount == null || EndingAccount == null)
                {
                    return 0;
                }
                return Math.Abs(12 * (OpeningAccount.Value.Year - EndingAccount.Value.Year) + OpeningAccount.Value.Month - EndingAccount.Value.Month);
            }
        }



        public void Debit(double moneyToSubstract)
        {
            if (moneyToSubstract < 0.0)
            {
                throw new ArgumentException("moneyToSubstract must be positive", "moneyToSubstract");
            }
            Balance -= moneyToSubstract;
        }

        public void Credit(double moneyToAdd)
        {
            if (moneyToAdd < 0.0)
            {
                throw new ArgumentException("moneyToAdd must be positive", "moneyToAdd");
            }
            Balance += moneyToAdd;
        }

        public void SetRate(double rateValue)
        {
            Rate = rateValue;
        }

        public void SetOpenningDate(DateTime openingDate)
        {
            this.OpeningAccount = openingDate;
            OnPeriodChange?.Invoke(this, new EventArgs());
        }

        public void SetEndingDate(DateTime closingDate)
        {
            this.EndingAccount = closingDate;
            OnPeriodChange?.Invoke(this, new EventArgs());
        }

        private void Account_OnPeriodChange(object sender, EventArgs e)
        {
            CheckPeriod();
        }

        private void CheckPeriod()
        {
            if(OpeningAccount == null || EndingAccount == null)
            {
                return;
            }

            if(OpeningAccount > EndingAccount)
            {
                throw new InvalidPeriodException();
            }

        }

        public void Calculation()
        {
            double res = Balance;
            if (res < CeillingValue)
            {
                res += res * (Rate / 100);
            }
            else
            {
                res += CeillingValue * (Rate / 100);
            }
            Balance = res;
        }

        public virtual AccountType Type
        {
            get
            {
                return AccountType.Base;
            }
        }
    }
}