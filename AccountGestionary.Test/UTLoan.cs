﻿using AccountGestionary.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AccountGestionary.Test
{
    public class UTLoan 
    {
        private readonly int capitalDefault = 100000;
        private readonly string ibanDefault = "FR8117569000505724247692C04";
        private readonly Loan loan;

        public UTLoan()
        {
            loan = new Loan(ibanDefault, capitalDefault);
        }


        [Fact]
        public void LoanIsChildOfAccount()
        {
            Assert.True(typeof(Account).IsInstanceOfType(loan));
        }


        [Fact]
        public void CheckMensualiteBaseRate()
        {
            Assert.Equal(Loan.BaseRate, loan.Rate);
        }

        [Fact]
        public void CheckAssuranceBaseRate()
        {
            Assert.Equal(Loan.AssuranceBaseRate, loan.AssuranceRate);
        }

        [Fact]
        public void CheckMensualiteRate1()
        {
            var openningDate = DateTime.Now.AddYears(-10);
            loan.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            loan.SetEndingDate(closingDate);
            Assert.Equal(Loan.LoansRate[(int)loan.TypeOfRate,1], loan.NominalRate, 5);
        }
        [Fact]
        public void CheckMensualiteRate2()
        {
            var openningDate = DateTime.Now.AddYears(-17);
            loan.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            loan.SetEndingDate(closingDate);
            Assert.Equal(Loan.LoansRate[(int)loan.TypeOfRate, 2], loan.NominalRate, 5);
        }

        [Fact]
        public void CheckMensualiteRate3()
        {
            loan.SetTypeOfRate(Loan.TypeRate.AwesomeRate);
            var openningDate = DateTime.Now.AddYears(-8);
            loan.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            loan.SetEndingDate(closingDate);
            Assert.Equal(Loan.LoansRate[(int)loan.TypeOfRate, 0], loan.NominalRate, 5);
        }

        [Fact]
        public void CheckAssuranceRate1()
        {
            Assert.Equal(Loan.AssuranceBaseRate, loan.AssuranceRate);
        }



        [Fact]
        public void CheckAssuranceRate2()
        {
            loan.IsSporty(true);
            loan.IsInArmy(true);
            loan.IsSmoker(true);
            Assert.Equal(0.55, loan.AssuranceRate);
        }

        [Fact]
        public void CheckAssuranceRate3()
        {
            loan.IsITEngineer(true);
            loan.HasHeartProblems(true);
            Assert.Equal(0.55, loan.AssuranceRate, 5);
        }


        [Fact]
        public void SetUpDiffThrowException()
        {
            Assert.Throws<InvalidPeriodException>(() => loan.SetDiffTime(DateTime.Now.AddYears(1)));
        }

        [Fact]
        public void SetUpDiffInvalidPeriod()
        {
            var openningDate = DateTime.Now.AddYears(-17);
            loan.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            loan.SetEndingDate(closingDate);
            Assert.Throws<InvalidPeriodException>(() => loan.SetDiffTime(DateTime.Now.AddYears(1)));
        }

        [Fact]
        public void SetUpDiff()
        {
            var diffDate = DateTime.Now.AddYears(-16);
            var openningDate = DateTime.Now.AddYears(-17);
            loan.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            loan.SetEndingDate(closingDate);
            loan.SetDiffTime(diffDate);
            Assert.Equal(diffDate, loan.DiffTimeDate);
        }

        [Fact]
        public void DefaultTypeOfRate()
        {
            Assert.Equal(Loan.BaseTypeRate, loan.TypeOfRate);
        }

        [Fact]
        public void DefaultOtherRate()
        {
            var type = Loan.TypeRate.AwesomeRate;
            loan.SetTypeOfRate(type);
            Assert.Equal(type, loan.TypeOfRate);
        }

        [Fact]
        public void DefaultMensuality()
        {
            Assert.True(loan.Mensualities.Count == 0);
        }

        [Fact]
        public void DefaultLoanScenarion()
        {
            var openningDate = DateTime.Now.AddYears(-17);
            loan.SetOpenningDate(openningDate);
            var closingDate = DateTime.Now;
            loan.SetEndingDate(closingDate);
            loan.Calculation();

            Assert.True(loan.Mensualities.Count != 0);
        }

    }
}
