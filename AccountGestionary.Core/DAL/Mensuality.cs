﻿using System;
using System.Collections.Generic;

namespace AccountGestionary.Core
{
    public class Mensuality
    {
        public Mensuality(int actualMonth, double capitalRemaining, double capitalRefund,double refundWithoutAssurance, double assurance, double interest)
        {
            ActualMonth = actualMonth;
            CapitalRemaining = Math.Round(capitalRemaining,2);
            CapitalRefund = Math.Round(capitalRefund,2);
            BaseRefund = Math.Round(refundWithoutAssurance - interest,2) < 0 ? 0.0 : Math.Round(refundWithoutAssurance - interest, 2);
            RefundWithoutAssurance = Math.Round(refundWithoutAssurance, 2);
            Assurance = Math.Round(assurance, 2);
            Interest = Math.Round(interest, 2);
            TotalMensuality = Math.Round(assurance + BaseRefund + interest, 2);
        }

        public int ActualMonth { get; private set; }
        public double CapitalRemaining { get; private set; }
        public double CapitalRefund { get; private set; }
        public double BaseRefund { get; private set; }
        public double Interest { get; private set; }
        public double RefundWithoutAssurance { get; private set; }
        public double Assurance { get; private set; } 
        public double TotalMensuality { get; set; }

    }
}