﻿using AccountGestionary.Core;
using AccountGestionary.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFClient
{
    /// <summary>
    /// Logique d'interaction pour LoanGestionary.xaml
    /// </summary>
    public partial class LoanGestionary : Window
    {
        public LoanGestionary()
        {
            InitializeComponent();
            lblDuration.Visibility = Visibility.Hidden;
            lblDurationTime.Visibility = Visibility.Hidden;
            dtpBegin.SelectedDate = new DateTime(2000, 1, 1);
            dtEnd.SelectedDate = new DateTime(2025, 1, 1);
            txtAccountBalance.Text = "200000";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Loan loan = GLoan.CreateLoan(txtIbanNumber.Text, double.Parse(txtAccountBalance.Text));

            loan.IsSporty(cbSport.IsChecked ?? false);
            loan.IsInArmy(cbPilote.IsChecked ?? false);
            loan.IsITEngineer(cbDev.IsChecked ?? false);
            loan.IsSmoker(cbFumeur.IsChecked ?? false);
            loan.HasHeartProblems(CbCardiaque.IsChecked ?? false);

            DateTime opdate = dtpBegin.SelectedDate ?? DateTime.Now;
            loan.SetOpenningDate(opdate);
            DateTime endDate = dtEnd.SelectedDate ?? DateTime.Now;
            loan.SetEndingDate(endDate);
            loan.SetDiffTime(dtpDiff.SelectedDate);
            loan.Calculation();


        }

        private void dgAccount_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Loan l = (Loan)dgAccount.SelectedItem;
            if(l != null)
            {
                dgMensuality.ItemsSource = l.Mensualities;
            }
            
        }
    }
}
