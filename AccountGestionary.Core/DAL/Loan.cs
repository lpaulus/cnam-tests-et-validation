﻿using System;
using System.Collections.Generic;

namespace AccountGestionary.Core
{
    public class Loan : Account, ICalculation
    {
        public enum TypeRate
        {
            GoodRate = 0,
            GreatRate = 1,
            AwesomeRate = 2
        }

        public Loan(string ibanDefault, double capital) : base(ibanDefault)
        {
            Balance = capital;
            OnPeriodChange += Loan_OnPeriodChange;
        }

        public Loan(string ibanDefault, double capital, TypeRate type) : base(ibanDefault)
        {
            Balance = capital;
            TypeOfRate = type;
            OnPeriodChange += Loan_OnPeriodChange;
        }

        public static TypeRate BaseTypeRate { get; } = TypeRate.GreatRate;
        public TypeRate TypeOfRate { get; private set; } = BaseTypeRate;
        public static double AssuranceBaseRate { get; } = 0.3;
        public double AssuranceRate { get; private set; } = AssuranceBaseRate;
        public double NominalRate { get; private set; }
        public DateTime? DiffTimeDate { get; set; } = null;
        public List<Mensuality> Mensualities { get; } = new List<Mensuality>();

        public static readonly double[,] LoansRate = new double[,]
        {
            { 0.62,0.67,0.85,1.04,1.27 },
            { 0.43,0.55,0.73,0.91,1.15 },
            { 0.35, 0.45,0.58,0.73,0.89 }
        };


        public void SetDiffTime(DateTime? dateTime)
        {
            if(!dateTime.HasValue)
            {
                DiffTimeDate = null;
                return;
            }

            if (OpeningAccount == null || EndingAccount == null || dateTime.Value < OpeningAccount || dateTime.Value > EndingAccount)
            {
                throw new InvalidPeriodException();
            }
            DiffTimeDate = dateTime;
        }

        public List<Mensuality> GetMensualites()
        {
            return new List<Mensuality>();
        }

        private void SetRate()
        {
            if (OpeningAccount == null || EndingAccount == null)
            {
                return;
            }

            if (DurationInYears >= 7 && DurationInYears < 10)
            {
                NominalRate = LoansRate[(int)TypeOfRate, 0];
            }
            else if (DurationInYears >= 10 && DurationInYears < 15)
            {
                NominalRate = LoansRate[(int)TypeOfRate, 1];
            }
            else if (DurationInYears >= 15 && DurationInYears < 20)
            {
                NominalRate = LoansRate[(int)TypeOfRate, 2];
            }
            else if (DurationInYears >= 20 && DurationInYears < 25)
            {
                NominalRate = LoansRate[(int)TypeOfRate, 3];
            }
            else if (DurationInYears >= 25)
            {
                NominalRate = LoansRate[(int)TypeOfRate, 4];
            }
            else
            {
                // Under Seven Years impossible
                throw new InvalidPeriodException();
            }
        }

        public void IsSporty(bool value)
        {
            if (value)
            {
                AssuranceRate -= 0.05;
            }
        }

        public void IsSmoker(bool value)
        {
            if (value)
            {
                AssuranceRate += 0.15;
            }
        }

        public void HasHeartProblems(bool value)
        {
            if (value)
            {
                AssuranceRate += 0.3;
            }
        }

        public void IsITEngineer(bool value)
        {
            if (value)
            {
                AssuranceRate -= 0.05;
            }
        }

        public void SetTypeOfRate(TypeRate rate)
        {
            TypeOfRate = rate;
            SetRate();
        }

        public void IsInArmy(bool value)
        {
            if (value)
            {
                AssuranceRate += 0.15;
            }

        }



        private void Loan_OnPeriodChange(object sender, EventArgs e)
        {
            
            SetRate();
            Calculation();
        }

        public new void Calculation()
        {
            if (OpeningAccount == null || EndingAccount == null)
            {
                return;
            }

            Mensualities.Clear();
            double paid = 0.0;
            for (int i=1; i<=DurationInMonth;i++)
            {

                double balanceRest = Balance - paid;
                double yearRate = (NominalRate / (100 * 12));
                double capitalXNominal = Balance * yearRate;

                double factor;
                if (DiffTimeDate == null)
                {
                    factor = 1 - Math.Pow(1 + yearRate, -DurationInMonth);
                }
                else
                {
                    int NombreMonth = Math.Abs(12 * (OpeningAccount.Value.Year - DiffTimeDate.Value.Year) + OpeningAccount.Value.Month - DiffTimeDate.Value.Month);
                    factor = 1 - Math.Pow(1 + yearRate, -(DurationInMonth - NombreMonth));
                }
                

                double mensualityWithoutAssurance = capitalXNominal / factor;
                double assurance = ((AssuranceRate / 100) * Balance) / 12;
                double interest = ((NominalRate / 100) / 12) * balanceRest;

                if (DiffTimeDate == null || !IsInDiffPeriod(i) )
                {
                    paid += mensualityWithoutAssurance - interest; 
                }
                else 
                {
                    mensualityWithoutAssurance = 0;
                }

                Mensuality mensuality = new Mensuality(i, balanceRest, paid, mensualityWithoutAssurance, assurance, interest);
                Mensualities.Add(mensuality);
            }
        }

        private bool IsInDiffPeriod(int nbrMonths)
        {
            var BeginDateCopy = OpeningAccount;
            BeginDateCopy = BeginDateCopy.Value.AddMonths(nbrMonths);
            if (BeginDateCopy < DiffTimeDate)
            {
                return true;
            }
            return false;
        }

        public virtual AccountType Type
        {
            get
            {
                return AccountType.Loan;
            }
        }
    }
}