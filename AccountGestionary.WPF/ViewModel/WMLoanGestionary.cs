﻿using AccountGestionary.Core;
using System.Collections.Generic;

namespace AccountGestionary.WPF.ViewModel
{
    class WMLoantGestionary
    {

        public IEnumerable<Loan> Loans
        {
            get
            {
                return GLoan.Loans;
            }
        }

    }
}
